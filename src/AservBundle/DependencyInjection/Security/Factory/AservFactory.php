<?php

namespace Mojomaja\Bundle\AservBundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\FormLoginFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

class AservFactory extends FormLoginFactory
{
    public function getKey()
    {
        return 'mojomaja_aserv';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
	$provider = 'security.authentication.provider.mojomaja_aserv.'.$id;
        $container
            ->setDefinition($provider, new DefinitionDecorator('security.authentication.provider.mojomaja_aserv'))
            ->replaceArgument(1, new Reference($userProviderId))
            ->replaceArgument(2, $id)
        ;

        return $provider;
    }
}

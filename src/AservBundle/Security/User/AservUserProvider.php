<?php

namespace Mojomaja\Bundle\AservBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Mojomaja\Bundle\AservBundle\Aid\AservAid;
use Mojomaja\Bundle\AservBundle\Aid\UserAidInterface;
use Mojomaja\Component\Aserv\User;

class AservUserProvider implements UserProviderInterface
{
    /**
     * @var \Mojomaja\Bundle\AservBundle\Aid\AservAid
     */
    private $aserv;

    /**
     * @var \Mojomaja\Bundle\AservBundle\Aid\UserAidInterface
     */
    private $user;

    private $class;


    public function __construct(AservAid $aserv, UserAidInterface $user, $class)
    {
        $this->aserv    = $aserv;
        $this->user     = $user;
        $this->class    = $class;
    }

    public function loadUserByUsername($username)
    {
        if (!($username instanceof User)) {
            $username = $this->aserv->lookup($username);

            if (!$username)
                throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }

        return $this->user->emerge($username);
    }

    public function refreshUser(UserInterface $user)
    {
        if (!($user instanceof $this->class))
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));

        return $this->user->refresh($user);
    }

    public function supportsClass($class)
    {
        return $class === $this->class || is_subclass_of($class, $this->class);
    }
}

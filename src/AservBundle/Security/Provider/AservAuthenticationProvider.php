<?php

namespace Mojomaja\Bundle\AservBundle\Security\Provider;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Mojomaja\Bundle\AservBundle\Security\User\AservUserProvider;
use Mojomaja\Component\Aserv\Client;
use Mojomaja\Component\Aserv\Exception;

class AservAuthenticationProvider implements AuthenticationProviderInterface
{
    /**
     * @var \Mojomaja\Component\Aserv\Client
     */
    private $aserv;

    /**
     * @var \Mojomaja\Bundle\AservBundle\Security\User\AservUserProvider
     */
    private $userProvider;

    private $providerKey;


    public function __construct(Client $aserv, AservUserProvider $userProvider, $providerKey)
    {
        $this->aserv        = $aserv;
        $this->userProvider = $userProvider;
        $this->providerKey  = $providerKey;
    }

    public function authenticate(TokenInterface $token)
    {
	try {
	    $user = $token->getUser();
	    if (!$token->getRoles()) {
		$login  = $this->aserv->login($token->getUsername(), $token->getCredentials());
		$user   = $this->userProvider->loadUserByUsername($login);
	    }

	    $aToken = new UsernamePasswordToken(
                $user, null, $token->getProviderKey(), $user->getRoles()
            );
	    $aToken->setAttribute('token', $this->aserv->getToken());

	    return $aToken;
	}
	catch (Exception $e) {
	    throw new BadCredentialsException('Bad credentials', 0, $e);
	}
    }

    public function supports(TokenInterface $token)
    {
	return
            $token instanceof UsernamePasswordToken &&
            $token->getProviderKey() === $this->providerKey
        ;
    }
}

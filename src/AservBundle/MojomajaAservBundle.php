<?php

namespace Mojomaja\Bundle\AservBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Mojomaja\Bundle\AservBundle\DependencyInjection\Security\Factory\AservFactory;

class MojomajaAservBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
	parent::build($container);

	/**
	 * @var $extension \Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension
	 */
	$extension = $container->getExtension('security');
	$extension->addSecurityListenerFactory(new AservFactory());
    }
}

<?php

namespace Mojomaja\Bundle\AservBundle\Aid;

use Symfony\Component\Security\Core\User\UserInterface;
use Mojomaja\Component\Aserv\User;

interface UserAidInterface
{
    function emerge(User $user);

    /**
     * @throws \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     */
    function refresh(UserInterface $user);
}

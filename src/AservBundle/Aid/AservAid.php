<?php

namespace Mojomaja\Bundle\AservBundle\Aid;

use Mojomaja\Component\Aserv\Client;

class AservAid
{
    /**
     * @var \Mojomaja\Component\Aserv\Client
     */
    private $aserv;


    public function __construct(Client $aserv)
    {
        $this->aserv    = $aserv;
    }

    public function lookup($ident, $active = true)
    {
        return current($this->aserv->search(array($ident), $active));
    }

    public function retrieve($id, $active = true)
    {
        return current($this->aserv->fetch(array($id), $active));
    }
}
